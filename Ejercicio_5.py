""" -----------------------Autor Christian Condoy-------------------------------
---------------------Email christian.condoy@unl.edu.ec--------------------------


Ejercicio 5: Escribe un programa que le pida al usuario una temperatura
en grados Celsius, la convierta a grados Fahrenheit e imprima
por pantalla la temperatura convertida.
"""
celcius = float(input("Ingrese el valor a convertir en grados celcius: "))
Fahrenheid = (celcius * 9/5) + 32

print(f"El resultante en Farenheit es: {Fahrenheid}")
